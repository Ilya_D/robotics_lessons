// объявляем постоянные переменные
// "привязываем" их к пинам
const int LED_1 = 8;
const int LED_2 = 9;
const int LED_3 = 10;
const int LED_4 = 11;
const int LED_5 = 12;
const int LED_6 = 13;
const int POT = A0;

void setup() {
  //настройка пинов как выход сигнала с Arduino
  pinMode (LED_1, OUTPUT);
  pinMode (LED_2, OUTPUT);
  pinMode (LED_3, OUTPUT);
  pinMode (LED_4, OUTPUT);
  pinMode (LED_5, OUTPUT);
  pinMode (LED_6, OUTPUT);
  pinMode (POT, INPUT);
  // включаем серийный порт на скорости (9600) для обмена данными
  Serial.begin (9600);
}

void loop() {
 
  // включаем 1й и ждем пока он горит
  digitalWrite (LED_1, HIGH);
  Serial.print ("включен 1й");
  Serial.print (", время включения = ");
  Serial.println (analogRead(POT));
  delay (analogRead(POT));

  // выключаем текущий и включаем следующий
  digitalWrite (LED_1, LOW);
  digitalWrite (LED_2, HIGH);
  Serial.println ("включен 2й");
  Serial.print (", время включения = ");
  Serial.println (analogRead(POT));
  delay (analogRead(POT));

  // выключаем текущий и включаем следующий
  digitalWrite (LED_2, LOW);
  digitalWrite (LED_3, HIGH);
  Serial.println ("включен 3й");
  Serial.print (", время включения = ");
  Serial.println (analogRead(POT));
  delay (analogRead(POT));

  // выключаем текущий и включаем следующий
  digitalWrite (LED_3, LOW);
  digitalWrite (LED_4, HIGH);
  Serial.println ("включен 4й");
  Serial.print (", время включения = ");
  Serial.println (analogRead(POT));
  delay (analogRead(POT));

  // выключаем текущий и включаем следующий
  digitalWrite (LED_4, LOW);
  digitalWrite (LED_5, HIGH);
  Serial.println ("включен 5й");
  Serial.print (", время включения = ");
  Serial.println (analogRead(POT));
  delay (analogRead(POT));

  // выключаем текущий и включаем следующий
  digitalWrite (LED_5, LOW);
  digitalWrite (LED_6, HIGH);
  Serial.println ("включен 6й");
  Serial.print (", время включения = ");
  Serial.println (analogRead(POT));
  delay (analogRead(POT));

  // идем в обратную сторону

  // выключаем текущий и включаем следующий
  digitalWrite (LED_6, LOW);
  digitalWrite (LED_5, HIGH);
  Serial.println ("включен 5й");
  Serial.print (", время включения = ");
  Serial.println (analogRead(POT));
  delay (analogRead(POT));

  // выключаем текущий и включаем следующий
  digitalWrite (LED_5, LOW);
  digitalWrite (LED_4, HIGH);
  Serial.println ("включен 4й");
  Serial.print (", время включения = ");
  Serial.println (analogRead(POT));
  delay (analogRead(POT));

  // выключаем текущий и включаем следующий
  digitalWrite (LED_4, LOW);
  digitalWrite (LED_3, HIGH);
  Serial.println ("включен 3й");
  Serial.print (", время включения = ");
  Serial.println (analogRead(POT));
  delay (analogRead(POT));

  // выключаем текущий и включаем следующий
  digitalWrite (LED_3, LOW);
  digitalWrite (LED_2, HIGH);
  Serial.println ("включен 2й");
  Serial.print (", время включения = ");
  Serial.println (analogRead(POT));
  delay (analogRead(POT));

  // выключаем последний и начинаем цикл loop заново
  digitalWrite (LED_2, LOW);
}
