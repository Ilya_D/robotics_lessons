const int JOY_X = A0;
const int JOY_Y = A1;
const int JOY_BUT = 2;

void setup() {
  Serial.begin (9600);
  pinMode (JOY_X, INPUT);
  pinMode (JOY_Y, INPUT);
  pinMode (JOY_BUT, INPUT);
}

void loop() {
  int pos_x = analogRead (JOY_X);
  int pos_y = analogRead (JOY_Y);
  int button_state = digitalRead (JOY_BUT);
  Serial.print ("положение по вертикали = ");
  Serial.println (pos_x);
  Serial.print ("положение по горизонтали = ");
  Serial.println (pos_y);
  Serial.print ("состояние кнопки = ");
  Serial.println (button_state);
  Serial.println ("=+=+=+=+=+=+=+=+=+=+");
  delay (500);
}
