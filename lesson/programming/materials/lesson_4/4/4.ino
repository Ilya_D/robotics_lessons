// объявляем постоянные переменные со своими именами
// "привязываем" их к пинам
const int BUTTON_1 = 3;
const int BUTTON_2 = 2;

void setup() {
  // настройка пинов на выход или вход сигналов 
  // и подтягивание встроенных резисторов
  pinMode (BUTTON_1, INPUT_PULLUP);
  pinMode (BUTTON_2, INPUT_PULLUP);
  // включаем серийный порт на скорости (9600) для обмена данными
  Serial.begin (9600);
}

void loop() {
  // создание переменных и запись в них результатов чтения пинов
  bool button_1_state = digitalRead (BUTTON_1);
  bool button_2_state = digitalRead (BUTTON_2);
  // действия после "записи" сигналов
  if (!button_1_state && button_2_state) {
    Serial.println ("Нажата кнопка №1");
  }
  if (!button_2_state && button_1_state) {
    Serial.println ("Нажата кнопка №2");
  }
  if (!button_1_state && !button_2_state) {
    Serial.println ("Нажаты обе кнопки");
  }
}
