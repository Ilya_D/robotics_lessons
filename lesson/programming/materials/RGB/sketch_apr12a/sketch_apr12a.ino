const int RED = 11;
const int GREEN = 10;
const int BLUE = 9;
int val = 0;

void setup() {
  pinMode (RED, OUTPUT);
  pinMode (GREEN, OUTPUT);
  pinMode (BLUE, OUTPUT);
}

void loop() {
  while (val < 255) {
    analogWrite (RED, val);
    val++;
    delay (10);
  }
  while (val > 0) {
    analogWrite (RED, val);
    val--;
    delay (10);
  }
}
