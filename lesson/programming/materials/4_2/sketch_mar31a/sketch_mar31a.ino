const int RED = 2;
const int YELLOW = 3;
const int GREEN = 4;
const int BUTTON = 7;

void setup(){
  Serial.begin(9600);
  pinMode (RED, OUTPUT);
  pinMode (YELLOW, OUTPUT);
  pinMode (GREEN, OUTPUT);
  pinMode (BUTTON, INPUT_PULLUP);
}

void loop(){
  digitalWrite (RED, HIGH);
  int button_state = digitalRead (BUTTON);
  Serial.println (button_state);
  if (button_state == 1) {
    
  }
}
