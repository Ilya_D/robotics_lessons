const int RED = 4;
const int YELLOW = 3;
const int GREEN = 2;
const int BUTTON = 7;

void setup(){
  Serial.begin(9600);
  pinMode (RED, OUTPUT);
  pinMode (YELLOW, OUTPUT);
  pinMode (GREEN, OUTPUT);
  pinMode (BUTTON, INPUT_PULLUP);
}

void loop(){
  digitalWrite (RED, HIGH);
  int button_state = digitalRead (BUTTON);
  Serial.println (button_state);
  if (button_state == 0) {
    digitalWrite (RED, LOW);
    digitalWrite (YELLOW, HIGH);
    delay (500);
    digitalWrite (YELLOW, LOW);
    delay (500);
    digitalWrite (YELLOW, HIGH);
    delay (500);
    digitalWrite (YELLOW, LOW);
    delay (500);
    digitalWrite (GREEN, HIGH);
    delay (5000);
    digitalWrite (GREEN, LOW);
  }
}
