// объявляем постоянные переменные
// "привязываем" их к пинам
const int RED = 11;
const int GREEN = 10;
const int BLUE = 9;

void setup() {
  //настройка пинов как выход сигнала с Arduino
  pinMode (RED, OUTPUT);
  pinMode (GREEN, OUTPUT);
  pinMode (BLUE, OUTPUT);
  // включаем серийный порт на скорости (9600) для обмена данными
  Serial.begin (9600);
}

void loop() {
  analogWrite (RED, 255);
}
