// объявляем постоянные переменные
// "привязываем" их к пинам
const int LED_RED = 4;
const int LED_YELLOW = 3;
const int LED_GREEN = 2;

void setup() {
  //настройка пинов как выход сигнала с Arduino
  pinMode (LED_RED, OUTPUT);
  pinMode (LED_YELLOW, OUTPUT);
  pinMode (LED_GREEN, OUTPUT);
  // включаем серийный порт на скорости (9600) для обмена данными
  Serial.begin (9600);
}

void loop() {
  // включаем красный и ждем пока он горит
  digitalWrite (LED_RED, HIGH);
  Serial.println ("включен красный");
  delay (5000);

  // выключаем красный и начинаем моргать желтым
  digitalWrite (LED_RED, LOW);
  Serial.println ("вsключен красный");

  // начинаем моргать желтым 3 раза
  Serial.println ("моргаем желтым 3 раза");
  digitalWrite (LED_YELLOW, HIGH);
  delay (500);
  digitalWrite (LED_YELLOW, LOW);
  delay (500);
  digitalWrite (LED_YELLOW, HIGH);
  delay (500);
  digitalWrite (LED_YELLOW, LOW);
  delay (500);
  digitalWrite (LED_YELLOW, HIGH);
  delay (500);
  digitalWrite (LED_YELLOW, LOW);
  delay (500);

  // включаем зеленый и ждем пока он горит
  digitalWrite (LED_GREEN, HIGH);
  Serial.println ("включен зеленый");
  delay (5000);

  // выключаем зеленый
  digitalWrite (LED_GREEN, LOW);
  Serial.println ("выключен зеленый");
}
