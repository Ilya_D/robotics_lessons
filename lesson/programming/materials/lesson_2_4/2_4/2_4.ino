// создаем глобальные переменные
// привязываем их к пинам Arduino
const int LED = 2;
const int BUTTON = 8;

void setup() {
  // настраиваем пины как выходы/входы для сигналов
  pinMode (LED, OUTPUT);
  pinMode (BUTTON, INPUT);
  Serial.begin (9600);  // включаем монитор порта
  Serial.println (" привет! "); // выводим в монитор порта приветствие
  delay (2000);
}

void loop() {
  // выводим в монитор порта состояние кнопки
  Serial.println (" состояние кнопки =  "); // выводим в монитор порта приветствие
  Serial.print (digitalRead (BUTTON));
  digitalWrite (LED, digitalRead (BUTTON));
  delay (100);
}
