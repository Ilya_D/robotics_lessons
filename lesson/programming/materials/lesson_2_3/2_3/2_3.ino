// создаем глобальные переменные
// привязываем их к пинам Arduino
const int LED_1 = 2;
const int LED_2 = 3;

void setup() {
  // настраиваем пины как выходы для сигнала
  pinMode (LED_1, OUTPUT);
  pinMode (LED_2, OUTPUT);
}

void loop() {
  digitalWrite (LED_1, HIGH); //посылаем на светодиод "высокий" сигнал (+5v)
  delay (500); //ждем 0,5 секунды
  digitalWrite (LED_1, LOW); //посылаем на светодиод "низкий" сигнал (gnd)
  digitalWrite (LED_2, HIGH); //посылаем на светодиод "высокий" сигнал (+5v)
  delay (500); //ждем 0,5 секунды
  digitalWrite (LED_2, LOW); //посылаем на светодиод "низкий" сигнал (gnd)
}
