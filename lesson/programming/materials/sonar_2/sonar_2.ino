const int TRIG = 13;        // назначаем имя для Pin13
const int ECHO = 12;        // назначаем имя для Pin12
const int ZUM = 2;          // назначаем имя для зумера

void setup() {
  Serial.begin (9600);      // подключаем монитор порта
  pinMode(ZUM, OUTPUT);     // назначаем ZUM, как выход
  pinMode(TRIG, OUTPUT);    // назначаем trigPin, как выход
  pinMode(ECHO, INPUT);     // назначаем echoPin, как вход
}

void loop() {
  int duration;             // назначаем переменную "duration" для показаний датчика
  int cm;                   // назначаем переменную "cm" для показаний датчика
  digitalWrite(TRIG, LOW);  // изначально датчик не посылает сигнал
  delayMicroseconds(2);     // ставим задержку в 2 микросекунды

  digitalWrite(TRIG, HIGH); // посылаем сигнал
  delayMicroseconds(10);    // ставим задержку в 10 микросекунды
  digitalWrite(TRIG, LOW);  // выключаем сигнал

  duration = pulseIn(ECHO, HIGH); // включаем прием сигнала

  cm = duration / 58;       // вычисляем расстояние в сантиметрах

  Serial.print(cm);         // выводим расстояние в сантиметрах
  Serial.println(" cm");
  
  digitalWrite (ZUM, HIGH);
  delay (500);
  digitalWrite (ZUM, LOW);
  delay (cm);
}
