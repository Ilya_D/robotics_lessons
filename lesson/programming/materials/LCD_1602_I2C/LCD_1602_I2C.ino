#include <Wire.h> 
#include <LiquidCrystal_I2C.h>
LiquidCrystal_I2C lcd(0x27,16,2);  // Устанавливаем дисплей
void setup()
{
  lcd.init();                     
  lcd.backlight();// Включаем подсветку дисплея
  lcd.setCursor(3,0);
  lcd.print("HELLO");
  lcd.setCursor(3, 1);
  lcd.print("NAME");
  delay(2000);
}
void loop()
{
  // Устанавливаем курсор на первую строку и нулевой символ.
  lcd.setCursor(0,0);
  lcd.print("I am working");
  // Устанавливаем курсор на вторую строку и нулевой символ.
  lcd.setCursor(0, 1);
  // Выводим на экран количество секунд с момента запуска ардуины
  lcd.print(millis()/1000);
  // выводим надпись "sek"
  lcd.setCursor(5, 1);
  lcd.print("sek");
}
