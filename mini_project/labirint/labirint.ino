#include <Servo.h>
// создаем глобальные переменные и привязываем их к пинам
const int JOY_X = A0;
const int JOY_Y = A1;
Servo myservo1;  // create servo object to control a servo
Servo myservo2;  // create servo object to control a servo
const int BUTTON = 3;
int val1 = 90;
int val2 = 90;

void setup() {
  // настраиваем наши пины на получение сигнала
  pinMode (BUTTON, INPUT_PULLUP);
  pinMode (JOY_X, INPUT);
  pinMode (JOY_Y, INPUT);
  // подключаем монитор порта
  Serial.begin (9600);

  myservo1.attach(5);  // attaches the servo on pin 9 to the servo object
  myservo2.attach(6);  // attaches the servo on pin 9 to the servo object
  myservo1.write(val1);
  myservo2.write(val2);

}

void loop() {
  // создаем переменные и записываем в них данные
  bool button_state = digitalRead (BUTTON);
  int joy_x_state = analogRead (JOY_X);
  int joy_y_state = analogRead (JOY_Y);
  // выводим данные в монитор порта
  Serial.print ("x = ");
  Serial.print (joy_x_state);
  Serial.print (" y = ");
  Serial.println (joy_y_state);
  Serial.print (" val1 = ");
  Serial.print (val1);
  Serial.print (" val2 = ");
  Serial.println (val2);
  Serial.print (" button = ");
  Serial.println (button_state);
  if (!button_state) {
    val1 = 90;
    val2 = 90;
    myservo1.write(val1);
    myservo2.write(val2);
    delay (50);
  }
  if (joy_x_state > 600 && val1 < 120) {
    myservo1.write(val1++);                  // sets the servo position according to the scaled value
  }
  if (joy_x_state < 400 && val1 > 60) {
    myservo1.write(val1--);                  // sets the servo position according to the scaled value
  }
  if (joy_y_state > 600 && val2 > 60) {
    myservo2.write(val2--);                  // sets the servo position according to the scaled value
  }
  if (joy_y_state < 400 && val2 < 120) {
    myservo2.write(val2++);                  // sets the servo position according to the scaled value
  }
  //delay(100);
}
