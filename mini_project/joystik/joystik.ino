// создаем глобальные переменные и привязываем их к пинам
const int JOY_X = A0;
const int JOY_Y = A1;

void setup() {
  // настраиваем наши пины на получение сигнала
  pinMode (JOY_X, INPUT);
  pinMode (JOY_Y, INPUT);
  // подключаем монитор порта
  Serial.begin (9600);
}

void loop() {
  // создаем переменные и записываем в них данные
  int joy_x_state = analogRead (JOY_X);
  int joy_y_state = analogRead (JOY_Y);
  // выводим данные в монитор порта
  Serial.print ("X = ");
  Serial.print (joy_x_state);
  Serial.print ("Y = ");
  Serial.println (joy_y_state);
  delay (500);
}
