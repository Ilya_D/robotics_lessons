# Robotics Lessons

## Уроки подключения и программирования Arduino

<https://gitlab.com/Ilya_D/robotics_lessons/-/tree/main/lesson/programming/ready>

## Уроки 3D моделирования

<https://gitlab.com/Ilya_D/robotics_lessons/-/tree/main/lesson/CAD>

## Уроки по электронике для начинающих

<https://gitlab.com/Ilya_D/robotics_lessons/-/tree/main/lesson/electronics>

## Уроки по пайке и монтажу

<https://gitlab.com/Ilya_D/robotics_lessons/-/tree/main/lesson/soldering>

